package com.practice.challenge_7.viewmodel.cart

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.practice.challenge_7.api.APIClient
import com.practice.challenge_7.database.cart.CartItemClass
import com.practice.challenge_7.database.cart.CartRepository
import com.practice.challenge_7.model.CartPost
import com.practice.challenge_7.model.OrderResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CartViewModel (private val repository: CartRepository): ViewModel() {
    val getAllCartItems: LiveData<List<CartItemClass>> = repository.getAllCartItems()
    val getTotalPrice: LiveData<Double> = repository.getTotalPrice()
    private val orderResult = MutableLiveData<Boolean>()


    fun insertCartItem(cartItem: CartItemClass){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val existingCartItem = repository.getCartItemByName(cartItem.foodTitle)

                if (existingCartItem != null){
                    existingCartItem.quantity =+ existingCartItem.quantity
                    existingCartItem.totalPrice = existingCartItem.basePrice?.times(existingCartItem.quantity)
                    updateCartItem(existingCartItem)
                } else {
                    repository.insertCartItem(cartItem)
                }
            }
        }
    }

    private fun updateCartItem(cartItem: CartItemClass){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                repository.updateCartItem(cartItem)
            }
        }
    }

    fun deleteEachCartItem(cartItem: CartItemClass){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                repository.deleteItem(cartItem)
            }
        }
    }

    fun countItem(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                repository.countItem()
            }
        }
    }

    fun addMenuQty(cartItem: CartItemClass){
        val menuQty = cartItem.quantity + 1
        cartItem.quantity = menuQty
        cartItem.totalPrice = cartItem.basePrice?.times(menuQty)

        updateCartItem(cartItem)
    }

    fun reduceMenuQty(cartItem: CartItemClass){
        val menuQty = cartItem.quantity - 1
        cartItem.quantity = menuQty
        cartItem.totalPrice = cartItem.basePrice?.times(menuQty)

        updateCartItem(cartItem)
    }

    fun order(orderData: CartPost){
        APIClient.instance
            .order(orderData)
            .enqueue(object : Callback<OrderResponse> {
                override fun onResponse(
                    call: Call<OrderResponse>,
                    response: Response<OrderResponse>
                ) {
                    if (response.isSuccessful){
                        orderResult.postValue(true)
                    }
                }

                override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                    t.message?.let {
                        Log.d("Failure", it)
                    }
                }
            })
    }
}