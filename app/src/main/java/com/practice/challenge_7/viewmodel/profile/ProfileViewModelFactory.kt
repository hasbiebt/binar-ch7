package com.practice.challenge_7.viewmodel.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.practice.challenge_7.database.profile.ProfileDAO

@Suppress("UNCHECKED_CAST")
class ProfileViewModelFactory(private val profileDAO: ProfileDAO): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
            return ProfileViewModel(profileDAO) as T
        }
        throw IllegalArgumentException("Unknown ViewModel Class")
    }
}