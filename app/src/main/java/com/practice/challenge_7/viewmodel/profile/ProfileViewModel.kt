package com.practice.challenge_7.viewmodel.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.practice.challenge_7.database.profile.ProfileClass
import com.practice.challenge_7.database.profile.ProfileDAO
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class ProfileViewModel(private val profileDAO: ProfileDAO): ViewModel() {
    var profileLiveData: LiveData<ProfileClass> = MutableLiveData()

    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    fun getUserByEmail() {
        val userEmail = Firebase.auth.currentUser?.email

        profileLiveData = userEmail!!.let { profileDAO.getProfile(userEmail) }
    }

    fun updateProfileUser(profile: ProfileClass) {
        executorService.execute { profileDAO.update(profile) }
    }

}