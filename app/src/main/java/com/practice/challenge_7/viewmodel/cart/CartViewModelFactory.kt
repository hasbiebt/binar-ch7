package com.practice.challenge_7.viewmodel.cart

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.practice.challenge_7.database.cart.CartDatabase
import com.practice.challenge_7.database.cart.CartRepository


@Suppress("UNCHECKED_CAST")
class CartViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
     override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CartViewModel::class.java)) {
            val cartDao = CartDatabase.getDatabase(application).cartItemDao()
            val repository = CartRepository(cartDao)
            return CartViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel")
    }
 }