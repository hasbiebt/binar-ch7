package com.practice.challenge_7.database.cart

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface CartDAO {
    @Insert
    suspend fun insert(data: CartItemClass)

    @Update
    suspend fun update(data: CartItemClass)

    @Delete
    suspend fun delete(data: CartItemClass)

    @Query("SELECT COUNT(*) FROM cart")
    fun countItem(): Int

    @Query("SELECT * FROM cart")
    fun getAllItems(): LiveData<List<CartItemClass>>

    @Query("SELECT * FROM cart WHERE foodTitle = :foodTitle LIMIT 1")
    suspend fun getCartItemByName(foodTitle: String): CartItemClass?


    @Query("SELECT SUM(totalPrice) FROM cart")
    fun getTotalPrice(): LiveData<Double>

    @Query("DELETE FROM cart WHERE id = :itemId")
    fun deleteItemById(itemId: Int): Int

    @Query("DELETE FROM cart")
    fun deleteAll()

}