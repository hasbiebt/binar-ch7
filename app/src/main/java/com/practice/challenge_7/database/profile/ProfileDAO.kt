package com.practice.challenge_7.database.profile

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface ProfileDAO {

    @Insert
    fun insert(data: ProfileClass)

    @Query("SELECT * FROM profile WHERE user_email = :email")
    fun getProfile(email: String): LiveData<ProfileClass>

    @Update
    fun update(data: ProfileClass)

}