package com.practice.challenge_7.database.cart

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "cart")
data class CartItemClass(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var foodTitle: String,
    var note: String?,
    var basePrice: Int? = 0,
    var totalPrice: Int? = 0,
    var quantity: Int = 1,
    var imgRes: String,
): Parcelable