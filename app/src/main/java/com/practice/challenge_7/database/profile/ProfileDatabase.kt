package com.practice.challenge_7.database.profile

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [ProfileClass::class], version = 2, exportSchema = false)
abstract class ProfileDatabase: RoomDatabase() {

    abstract fun profileClassDao(): ProfileDAO

    companion object {

        @Volatile
        private var INSTANCE: ProfileDatabase? = null

        fun getDatabase(context: Context): ProfileDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ProfileDatabase::class.java,
                    "profile_database"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }
}