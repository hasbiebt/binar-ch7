package com.practice.challenge_7.database.profile

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "profile")

data class ProfileClass(
    @PrimaryKey(autoGenerate = true)
    var user_id: Int = 0,
    var user_email: String,
    var user_phone_number: String
): Parcelable
