package com.practice.challenge_7.database.cart

import androidx.lifecycle.LiveData

class CartRepository(private var cartDao: CartDAO){

    fun getAllCartItems(): LiveData<List<CartItemClass>> = cartDao.getAllItems()

    suspend fun insertCartItem(cartItem: CartItemClass){
        cartDao.insert(cartItem)
    }

    suspend fun updateCartItem(cartItem: CartItemClass){
        cartDao.update(cartItem)
    }

    fun deleteAllCartItems(){
        cartDao.deleteAll()
    }

    fun countItem(){
        cartDao.countItem()
    }

    suspend fun deleteItem(cartItem: CartItemClass){
        cartDao.delete(cartItem)
    }

    fun getTotalPrice(): LiveData<Double> = cartDao.getTotalPrice()

    suspend fun getCartItemByName(foodTitle: String): CartItemClass? {
        return cartDao.getCartItemByName(foodTitle)
    }


}