package com.practice.challenge_7.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.practice.challenge_7.view.activity.LoginActivity
import com.practice.challenge_7.R
import com.practice.challenge_7.database.profile.ProfileDatabase
import com.practice.challenge_7.databinding.FragmentProfileBinding
import com.practice.challenge_7.viewmodel.profile.ProfileViewModel
import com.practice.challenge_7.viewmodel.profile.ProfileViewModelFactory

class ProfileFragment : Fragment() {
    private lateinit var binding: FragmentProfileBinding
    private lateinit var auth: FirebaseAuth
    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater, container, false)

        val db = ProfileDatabase.getDatabase(requireContext())
        val dao = db.profileClassDao()
        viewModel = ViewModelProvider(requireActivity(), ProfileViewModelFactory(dao))[ProfileViewModel::class.java]
        auth = Firebase.auth

        userProfile()
        editUserProfile()
        logoutUser()

        return binding.root
    }

    private fun userProfile(){
        viewModel.getUserByEmail()
        viewModel.profileLiveData.observe(viewLifecycleOwner){
            binding.profileEmailPlaceholder.text = it.user_email
            binding.profileUnamePlaceholder.text = it.user_email
            binding.profilePhoneNoPlaceholder.text = it.user_phone_number
        }
    }

    private fun editUserProfile(){
        binding.iconEdit.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_updateProfileFragment)
        }
    }

    private fun logoutUser(){
        binding.profileLogout.setOnClickListener{
            auth.signOut()
            startActivity(Intent(requireContext(), LoginActivity::class.java))
            activity?.finish()
        }
    }
}