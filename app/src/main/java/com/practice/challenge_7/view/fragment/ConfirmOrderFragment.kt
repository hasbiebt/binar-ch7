package com.practice.challenge_7.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.practice.challenge_7.adapter.CartMenuAdapter
import com.practice.challenge_7.databinding.FragmentConfirmOrderBinding
import com.practice.challenge_7.model.CartPost
import com.practice.challenge_7.model.Order
import com.practice.challenge_7.viewmodel.cart.CartViewModel
import com.practice.challenge_7.viewmodel.cart.CartViewModelFactory
import java.text.NumberFormat
import java.util.Currency

class ConfirmOrderFragment : Fragment() {
    private lateinit var binding: FragmentConfirmOrderBinding
    private lateinit var viewModel: CartViewModel
    private lateinit var cartMenuAdapter: CartMenuAdapter
    private val auth = FirebaseAuth.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentConfirmOrderBinding.inflate(inflater, container, false)

        setupViewModel()
        recyclerViewSetting()
        clickOrder()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeData()
        observeTotalPrice()



    }

    private fun recyclerViewSetting(){
        cartMenuAdapter = CartMenuAdapter(viewModel)

        binding.recyclerViewConfirmOrder.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerViewConfirmOrder.adapter = cartMenuAdapter
    }

    private fun setupViewModel(){
        viewModel = ViewModelProvider(
            this,
            CartViewModelFactory(requireActivity().application)
        )[CartViewModel::class.java]
    }

    private fun observeTotalPrice(){
        viewModel.getTotalPrice.observe(viewLifecycleOwner, Observer {totalPrice ->
            //format model harganya IDRXX,XXX
            if (totalPrice != null) {
                val formattedValue = formatCurrency(totalPrice, "IDR")
                binding.orderSummaryPrice.text = formattedValue
            } else {
                binding.orderSummaryPrice.text = "N/A"
            }
        })
    }

    //observe tiap item yang masuk
    private fun observeData(){
        viewModel.getAllCartItems.observe(viewLifecycleOwner, Observer {
            cartMenuAdapter.setData(it)
        })

        viewModel.getAllCartItems
    }

    private fun formatCurrency(value: Double, currencyCode: String): String {
        val format = NumberFormat.getCurrencyInstance().apply {
            maximumFractionDigits = 0
            currency = Currency.getInstance(currencyCode)
        }
        return format.format(value)
    }

    private fun clickOrder(){
        binding.buttonOrder.setOnClickListener {
            val username = auth.currentUser!!.email
            val orderItem = viewModel.getAllCartItems.value ?: emptyList()

            if (orderItem.isNotEmpty()) {
                // Mapping
                val itemOrders = orderItem.map {
                    Order(it.note ?: "", it.totalPrice ?: 0, it.foodTitle , it.quantity)
                }

                // Calculating total
                val totalPrice = orderItem.sumOf {
                    it.totalPrice ?: 0
                }

                // Creating CartPost instance
                val orderRequest = CartPost(totalPrice, username, itemOrders)

                viewModel.order(orderRequest)

                Toast.makeText(requireContext(), "Sukses Pesan!", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(requireContext(), "Data Kosong", Toast.LENGTH_SHORT).show()
            }
        }
    }
}