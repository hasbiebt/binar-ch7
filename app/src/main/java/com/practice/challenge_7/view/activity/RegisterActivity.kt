package com.practice.challenge_7.view.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.practice.challenge_7.database.profile.ProfileClass
import com.practice.challenge_7.database.profile.ProfileDatabase
import com.practice.challenge_7.databinding.ActivityRegisterBinding
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        auth = Firebase.auth

        binding.registerButton.setOnClickListener {
            register(binding.inputEmailRegister.text.toString(), binding.inputPasswordRegister.text.toString())
        }
    }

    private fun register(email: String, password: String) {
        val emailUser = binding.inputEmailRegister.text.toString()
        val mobileUser = binding.inputPhoneNumberRegister.text.toString()

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    insertProfile(emailUser, mobileUser)
                    Toast.makeText(this, "Register Success", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(
                        baseContext,
                        "Authentication failed.",
                        Toast.LENGTH_SHORT,
                    ).show()
                }
            }
    }

    private fun insertProfile(email: String, mobile: String) {
        val executorService: ExecutorService = Executors.newSingleThreadExecutor()

        val profile = ProfileClass(user_email = email, user_phone_number = mobile)

        val profileDAO = ProfileDatabase.getDatabase(application).profileClassDao()

        executorService.execute {
            profileDAO.insert(profile)
        }

    }
}