package com.practice.challenge_7.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.practice.challenge_7.adapter.CartMenuAdapter
import com.practice.challenge_7.databinding.FragmentCartBinding
import com.practice.challenge_7.viewmodel.cart.CartViewModel
import com.practice.challenge_7.viewmodel.cart.CartViewModelFactory
import java.text.NumberFormat
import java.util.Currency

class CartFragment : Fragment() {
    private lateinit var binding: FragmentCartBinding
    private lateinit var viewModel: CartViewModel
    private lateinit var cartMenuAdapter: CartMenuAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCartBinding.inflate(inflater, container, false)

        viewModel = ViewModelProvider(
            this,
            CartViewModelFactory(requireActivity().application)

        )[CartViewModel::class.java]

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cartMenuAdapter = CartMenuAdapter(viewModel)

        binding.recyclerViewCart.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerViewCart.adapter = cartMenuAdapter

        observeData()
        observeTotalPrice()
        navigateToConfirmOrder()
    }

    //pake live data buat observe total harga belanjaan
    private fun observeTotalPrice(){
        viewModel.getTotalPrice.observe(viewLifecycleOwner, Observer {totalPrice ->
            //format model harganya IDRXX,XXX
            if (totalPrice != null) {
                val formattedValue = formatCurrency(totalPrice, "IDR")
                binding.price.text = formattedValue
            } else {
                binding.price.text = "N/A"
            }
        })
    }

    //observe tiap item yang masuk
    private fun observeData(){
        viewModel.getAllCartItems.observe(viewLifecycleOwner, Observer {
            cartMenuAdapter.setData(it)
        })

        viewModel.getAllCartItems
    }

    private fun formatCurrency(value: Double, currencyCode: String): String {
        val format = NumberFormat.getCurrencyInstance().apply {
            maximumFractionDigits = 0
            currency = Currency.getInstance(currencyCode)
        }
        return format.format(value)
    }

    private fun navigateToConfirmOrder(){
        binding.confirmOrder.setOnClickListener {
            val action =
                com.practice.challenge_7.view.fragment.CartFragmentDirections.actionCartFragmentToConfirmOrderFragment()
            findNavController().navigate(action)
        }
    }

}