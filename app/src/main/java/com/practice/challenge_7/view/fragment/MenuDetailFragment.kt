package com.practice.challenge_7.view.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.practice.challenge_7.database.cart.CartItemClass
import com.practice.challenge_7.databinding.FragmentMenuDetailBinding
import com.practice.challenge_7.viewmodel.cart.CartViewModel
import com.practice.challenge_7.viewmodel.cart.CartViewModelFactory

class MenuDetailFragment : Fragment() {
    private lateinit var binding : FragmentMenuDetailBinding
    private lateinit var cartViewModel : CartViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMenuDetailBinding.inflate(inflater, container, false)

        cartViewModel = ViewModelProvider(
            this,
            CartViewModelFactory(requireActivity().application)
        )[CartViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        receiveArgsFromHomeFragment()
        locationMap()

        binding.addToCart.setOnClickListener{
            val args: MenuDetailFragmentArgs by navArgs()
            val newCartItems = CartItemClass(
                foodTitle = args.menu.nama,
                note = " ",
                basePrice = args.menu.harga,
                totalPrice = args.menu.harga,
                quantity = 1,
                imgRes = args.menu.imageUrl
            )

            cartViewModel.insertCartItem(newCartItems)
            val action =
                MenuDetailFragmentDirections.actionMenuDetailFragmentToCartFragment(
                    newCartItems
                )
            findNavController().navigate(action)
        }
    }

    //terima data dari fragment home screen
    private fun receiveArgsFromHomeFragment() {
        val args: MenuDetailFragmentArgs by navArgs()
        //update current data to the new ones
        binding.menuDetailTitle.text = args.menu.nama
        binding.menuDetailPrice.text = args.menu.hargaFormat
        binding.menuDetailDesc.text = args.menu.detail

        Glide.with(binding.menuImgHighlight)
            .load(args.menu.imageUrl)
            .fitCenter()
            .into(binding.menuImgHighlight)
    }

    private fun locationMap(){
        binding.storeLocation.setOnClickListener {
            val locationUri = "https://maps.app.goo.gl/h4wQKqaBuXzftGK77"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(locationUri))
            startActivity(intent)
        }
    }

}