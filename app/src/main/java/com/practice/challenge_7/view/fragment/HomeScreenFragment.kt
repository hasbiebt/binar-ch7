package com.practice.challenge_7.view.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.practice.challenge_7.adapter.FoodMenuAdapter
import com.practice.challenge_7.api.APIClient
import com.practice.challenge_7.databinding.FragmentHomeScreenBinding
import com.practice.challenge_7.model.MenuData
import com.practice.challenge_7.model.MenuResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeScreenFragment : Fragment() {
    private lateinit var binding: FragmentHomeScreenBinding
    private lateinit var sharedPreferences: SharedPreferences
    private var isGrid = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferences = requireContext().getSharedPreferences("layout_preference", Context.MODE_PRIVATE)
        isGrid = sharedPreferences.getBoolean("isGrid", true)

        getListMenu { menuList ->
            recyclerViewSetting(isGrid, menuList)
        }

        binding.iconLinear.setOnClickListener{
            isGrid =! isGrid
            saveUserPreference(isGrid)
            getListMenu { menuList ->
                recyclerViewSetting(isGrid, menuList)
            }
        }
    }

    private fun recyclerViewSetting(isGrid: Boolean, foodList: List<MenuData>?) {

        foodList?.let {
            binding.recyclerViewHome.adapter = FoodMenuAdapter(isGrid, foodList) { data ->
                val action =
                    com.practice.challenge_7.view.fragment.HomeScreenFragmentDirections.actionHomeScreenFragmentToMenuDetailFragment(
                        data
                    )
                findNavController().navigate(action)
            }
        }

        if (isGrid) {
            binding.recyclerViewHome.layoutManager = GridLayoutManager(requireContext(), 2)
        } else {
            binding.recyclerViewHome.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun saveUserPreference(isGrid: Boolean) {
        val editor = sharedPreferences.edit()
        editor.putBoolean("isGrid", isGrid)
        editor.apply()
    }

    private fun getListMenu(callback: (List<MenuData>?) -> Unit){
        APIClient.instance
            .getListMenu()
            .enqueue(object : Callback<MenuResponse> {
                override fun onResponse(
                    call: Call<MenuResponse>,
                    response: Response<MenuResponse>
                ) {
                    if (response.isSuccessful){
                        recyclerViewSetting(isGrid, response.body()?.data)
                    } else {
                        callback(null)
                    }
                }

                override fun onFailure(
                    call: Call<MenuResponse>,
                    t: Throwable
                ) {
                    t.message?.let { Log.d("Failed", it) }
                    callback(null)
                }
            })
    }
}