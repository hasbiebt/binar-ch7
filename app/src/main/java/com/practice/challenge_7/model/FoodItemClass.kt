package com.practice.challenge_7.model

import java.io.Serializable

data class FoodItemClass(
    val title: String,
    val price: Int,
    val imgResource: Int,
    val description: String
) : Serializable
