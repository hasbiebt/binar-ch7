package com.practice.challenge_7.model

data class OrderClass(
    val username: String,
    val total: Int,
    val orders: ArrayList<OrderItem>
)

data class OrderItem(
    val name: String,
    val qty: Int,
    val notes: String?,
    val price: Double
)