package com.practice.challenge_7.model

import com.google.gson.annotations.SerializedName


data class OrderResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("code")
    val code: Int
)