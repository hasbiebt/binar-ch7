package com.practice.challenge_7.model


import com.google.gson.annotations.SerializedName

data class Order(
    @SerializedName("catatan")
    val catatan: String,
    @SerializedName("harga")
    val harga: Int,
    @SerializedName("nama")
    val nama: String,
    @SerializedName("qty")
    val qty: Int
)