package com.practice.challenge_7.model

import com.google.gson.annotations.SerializedName

data class OrderResponseClass(
    @SerializedName("code")
    val code: Int?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("status")
    val status: Boolean?
)
