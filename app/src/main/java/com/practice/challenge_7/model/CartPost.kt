package com.practice.challenge_7.model


import com.google.gson.annotations.SerializedName

data class CartPost(
    @SerializedName("total")
    val total: Int,
    @SerializedName("username")
    val username: String?,
    @SerializedName("orders")
    val orders: List<Order>
)