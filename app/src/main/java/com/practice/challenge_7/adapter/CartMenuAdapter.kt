package com.practice.challenge_7.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.practice.challenge_7.database.cart.CartItemClass
import com.practice.challenge_7.viewmodel.cart.CartViewModel
import com.practice.challenge_7.databinding.CartMenuBinding

class CartMenuAdapter(
    private val cartViewModel: CartViewModel
): RecyclerView.Adapter<CartMenuAdapter.CartViewHolder>() {

    private var dataList: List<CartItemClass> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val cartMenuView = CartMenuBinding.inflate(inflater, parent, false)
        return CartViewHolder(cartMenuView)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        val cartItem = dataList[position]
        holder.bind(cartItem, viewModel = cartViewModel)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(dataList: List<CartItemClass>){
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class CartViewHolder(private var bindingCart: CartMenuBinding) : ViewHolder(bindingCart.root){
        private val imageView: ImageView = bindingCart.imgCartMenu
        private val titleTextView: TextView = bindingCart.titleCartMenu
        private val priceTextView: TextView = bindingCart.priceCartMenu
        private val qtyTextView: TextView = bindingCart.quantityCounter
        private val noteCartTextView: TextView = bindingCart.textInputField

        fun bind(data: CartItemClass, viewModel: CartViewModel) {
            // texts
            titleTextView.text = data.foodTitle
            priceTextView.text = data.basePrice.toString()
            qtyTextView.text = data.quantity.toString()
            noteCartTextView.text = data.note.toString()

            //images
            Glide.with(imageView.context)
                .load(data.imgRes)
                .centerCrop()
                .into(imageView)

            bindingCart.minusIcon.setOnClickListener{
                //cek datanya ke-update ga di dao
                try {
                    if (data.quantity > 0) { //harus di atas 0 jumlah item
                        viewModel.reduceMenuQty(data)
                        bindingCart.quantityCounter.text = data.quantity.toString()
                        Log.d("FoodItemQuantity", "Quantity: ${data.quantity}")
                    } else if (data.quantity == 0){
                        viewModel.deleteEachCartItem(data)
                    }
                } catch (e: Exception) {
                    Log.e("ReduceMenuQty", "Error reducing menu quantity", e)
                }
            }

            bindingCart.plusIcon.setOnClickListener{
                //cek datanya ke-update ga di dao
                try {
                    if (data.quantity > 0) { //harus di atas 0 jumlah item
                        viewModel.addMenuQty(data)
                        bindingCart.quantityCounter.text = data.quantity.toString()
                        Log.d("FoodItemQuantity", "Quantity: ${data.quantity}")
                    }
                } catch (e: Exception) {
                    Log.e("AddMenuQty", "Error adding menu quantity", e)
                }
            }

            bindingCart.trashIcon.setOnClickListener{
                viewModel.deleteEachCartItem(data)
            }
        }
    }
}

