package com.practice.challenge_7.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.practice.challenge_7.model.MenuData
import com.practice.challenge_7.databinding.GridMenuBinding
import com.practice.challenge_7.databinding.LinearMenuBinding

class FoodMenuAdapter(
    private var isGrid : Boolean,
    private var menuDataList : List<MenuData>,
    private var onItemClick: (menuData: MenuData) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if(isGrid) {
            val gridMenuView = GridMenuBinding.inflate(inflater, parent, false)
            GridMenuHolder(gridMenuView)
        } else {
            val linearMenuView = LinearMenuBinding.inflate(inflater, parent, false)
            LinearMenuHolder(linearMenuView)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val foodItem = menuDataList[position]

        if(isGrid){
            val gridHolder = holder as GridMenuHolder
            gridHolder.bind(foodItem, onItemClick)
        } else {
            val linearHolder = holder as LinearMenuHolder
            linearHolder.bind(foodItem, onItemClick)
        }
    }

    override fun getItemCount(): Int {
        return menuDataList.size
    }

    class GridMenuHolder(private var binding: GridMenuBinding) : ViewHolder(binding.root) {
        //change each of the view with the grid's view id
        private val imageView: ImageView = binding.menuImg
        private val titleTextView: TextView = binding.menuTitle
        private val priceTextView: TextView = binding.menuPrice

        fun bind(menuData: MenuData, onItemClick: (menuData: MenuData) -> Unit) {
            // Bind menuData to views here
            titleTextView.text = menuData.nama
            priceTextView.text = menuData.harga.toString()

            Glide.with(imageView.context)
                .load(menuData.imageUrl)
                .centerCrop()
                .into(imageView)

            val item = binding.root

            item.setOnClickListener {
                onItemClick.invoke(menuData)
            }
        }
    }

    class LinearMenuHolder(private val binding: LinearMenuBinding) : ViewHolder(binding.root) {
        private val imageView: ImageView = binding.foodItemImg
        private val titleTextView: TextView = binding.foodTitle
        private val priceTextView: TextView = binding.foodPrice

        fun bind(menuData: MenuData, onItemClick: (menuData: MenuData) -> Unit) {
            // Bind menuData to views here
            titleTextView.text = menuData.nama
            priceTextView.text = menuData.harga.toString()

            Glide.with(imageView.context)
                .load(menuData.imageUrl)
                .centerCrop()
                .into(imageView)

            val item = binding.root
            item.setOnClickListener {
                onItemClick.invoke(menuData)
            }
        }
    }
}